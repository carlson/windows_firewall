﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
 
namespace firewall
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        firewallOP myFirewallOP =new firewallOP();
        machineInfo myMachineInfo = new machineInfo();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if(myFirewallOP.IsAdministrator())
            {
                myFirewallOP.FirewallEnableByObject(false, false, false);
            }
            else
            {
                MessageBox.Show("need admin id");
            }
            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (myFirewallOP.IsAdministrator())
            {
                myFirewallOP.FirewallEnableByObject(true, true, true);
            }
            else
            {
                MessageBox.Show("need admin id");
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            myMachineInfo.getMachineInfo();
        }
    }
}

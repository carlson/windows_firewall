﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetFwTypeLib;
using System.Security.Principal;

namespace firewall
{
    class firewallOP
    {

        /// <summary>
        /// 判断程序是否拥有管理员权限
        /// </summary>
        /// <returns>true:是管理员；false:不是管理员</returns>
        public  bool IsAdministrator()
        {
            WindowsIdentity current = WindowsIdentity.GetCurrent();
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
            return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        /// <summary>
        /// 通过对象防火墙操作
        /// </summary>
        /// <param name="isOpenDomain">域网络防火墙（禁用：false；启用（默认）：true）</param>
        /// <param name="isOpenPublicState">公共网络防火墙（禁用：false；启用（默认）：true）</param>
        /// <param name="isOpenStandard">专用网络防火墙（禁用: false；启用（默认）：true）</param>
        /// <returns></returns>
        public  bool FirewallEnableByObject(bool isOpenDomain = true, bool isOpenPublicState = true, bool isOpenStandard = true)
        {
            try
            {
                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                // 启用<高级安全Windows防火墙> - 专有配置文件的防火墙
                firewallPolicy.set_FirewallEnabled(NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_PRIVATE, isOpenStandard);
                // 启用<高级安全Windows防火墙> - 公用配置文件的防火墙
                firewallPolicy.set_FirewallEnabled(NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_PUBLIC, isOpenPublicState);
                // 启用<高级安全Windows防火墙> - 域配置文件的防火墙
                firewallPolicy.set_FirewallEnabled(NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_DOMAIN, isOpenDomain);
            }
            catch (Exception e)
            {
                string error = $"防火墙修改出错：{e.Message}";
                throw new Exception(error);
            }
            return true;
        }
    }
}

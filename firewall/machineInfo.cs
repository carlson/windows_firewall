﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace firewall
{
    class machineInfo
    {
        /// <summary>
        /// 获取本机相关信息
        /// </summary>
       public void getMachineInfo()
        {
            StringBuilder lb_Mynfo = new StringBuilder() ;
            //清除items项
            lb_Mynfo.Clear();
            lb_Mynfo.AppendLine("----------------------------本机信息----------------------------");
            //获取本机名称
            lb_Mynfo.AppendLine("本机名称：" + Environment.MachineName);
            //获取系统版本号
            lb_Mynfo.AppendLine("系统版本号：" + Environment.OSVersion.VersionString);
 
            
            //获取本机IP地址
            lb_Mynfo.AppendLine("IP地址：");
            System.Net.IPAddress[] al = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList;

           

            for (int i = 0; i < al.Length; i++)
                lb_Mynfo.AppendLine(al[i].ToString());


            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            Console.WriteLine("tcp--------------------------------------------------");
            IPEndPoint[] endPointsTcp = properties.GetActiveTcpListeners();
            foreach (IPEndPoint e in endPointsTcp)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("udp--------------------------------------------------");
            IPEndPoint[] endPointsUdp = properties.GetActiveUdpListeners();
            foreach (IPEndPoint e in endPointsUdp)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("tcp connect--------------------------------------------------");

            TcpConnectionInformation[] tcpConnectionInformation = properties.GetActiveTcpConnections();
            foreach (TcpConnectionInformation tci in tcpConnectionInformation)
            {
                Console.WriteLine(tci.LocalEndPoint.ToString()+"---"+ tci.RemoteEndPoint.ToString());
            }


            IPGlobalStatistics ipstat = properties.GetIPv4GlobalStatistics();
            lb_Mynfo.AppendLine("本机所在域：" + properties.DomainName);
            lb_Mynfo.AppendLine("接收数据包：" + ipstat.ReceivedPackets);
            lb_Mynfo.AppendLine("转发数据包：" + ipstat.ReceivedPacketsForwarded);
            lb_Mynfo.AppendLine("传送数据包：" + ipstat.ReceivedPacketsDelivered);
            lb_Mynfo.AppendLine("丢弃数据包：" + ipstat.ReceivedPacketsDiscarded);

            Console.WriteLine(lb_Mynfo);
        }
    
          

    }
}
